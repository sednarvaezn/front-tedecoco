export const uploadFile = (file) =>
  fetch(`http://localhost:8080/api/v1/formulario`, {
    method: "POST",

    body: file,
  }).then(async (response) => {
    //const users = response.json();
    //console.log(users.body);
    if (response.ok) {
      return response.json();
    }
    //localStorage.clear();
    const r = await response.json();
    throw new Error(r.message);
  });

export const getAllById = (id) =>
  fetch(`http://localhost:8080/api/v1/formulario/${id}`, {
    method: "GET",
    headers: new Headers({
      "Content-Type": "application/json",
    }),
  }).then((response) => {
    //const users = response.json();
    //console.log(users.body);
    if (response.ok) {
      return response.json();
    }
    //localStorage.clear();
    throw new Error("Error");
  });

export const getAnswersById = (id) =>
  fetch(`http://localhost:8080/api/v1/respuestas/${id}`, {
    method: "GET",
    headers: new Headers({
      "Content-Type": "application/json",
    }),
  }).then((response) => {
    //const users = response.json();
    //console.log(users.body);
    if (response.ok) {
      return response.json();
    }
    //localStorage.clear();
    throw new Error("Error");
  });
