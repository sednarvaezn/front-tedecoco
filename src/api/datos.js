export const getCampos = (idFormulario) =>
  fetch(`http://localhost:8080/api/v1/campos/${idFormulario}`, {
    method: "GET",
    headers: new Headers({
      "Content-Type": "application/json",
    }),
  }).then((response) => {
    //const users = response.json();
    //console.log(users.body);
    if (response.ok) {
      return response.json();
    }
    //localStorage.clear();
    throw new Error("Error");
  });

export const create = async (body) =>
  await fetch(`http://localhost:8080/api/v1/respuestas`, {
    method: "POST",
    headers: new Headers({
      "Content-Type": "application/json",
    }),
    body: body,
  }).then((response) => {
    //const users = response.json();
    //console.log(users.body);
    if (response.ok) {
      return response.json();
    }
    //localStorage.clear();
    throw new Error("Error");
  });
