import { BrowserRouter, Route, Routes } from "react-router-dom";
import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/navbar.css";
import "../styles/footer.css";
import Formulario from "../pages/Formulario";
import Home from "../pages/Home";
import Select from "../pages/Select";
import CreateFormulario from "../pages/CreateFormulario";
import Navbar from "../layout/Navbar";
import Footer from "../layout/Footer";
import Visualizacion from "../pages/visualizacion";
import Respuestas from "../pages/respuestas";

function App() {
  return (
    <div className="content">
      <Navbar />
      <BrowserRouter>
        <Routes>
          <Route
            exact
            path="/create-formulario"
            element={<CreateFormulario />}
          />
          <Route exact path="/select" element={<Select />} />
          <Route exact path="/ver-respuestas" element={<Visualizacion />} />

          <Route
            exact
            path="/respuestas/:idFormulario"
            element={<Respuestas />}
          />
          <Route
            exact
            path="/formulario/:idFormulario"
            element={<Formulario />}
          />
          <Route exact path="/" element={<Home />} />
        </Routes>
      </BrowserRouter>
      <Footer />
    </div>
  );
}

export default App;
