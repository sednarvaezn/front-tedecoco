import React from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap';

export default () => {
  return (
    <Navbar bg="light" expand="lg" >
      <Container>
        <Navbar.Brand href="/">App Esquema Preconceptual</Navbar.Brand>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/select">Ver Formularios</Nav.Link>
            <Nav.Link href="/create-formulario">Crear Formulario</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}
