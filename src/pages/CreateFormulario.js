import React, { useState } from "react";
import { uploadFile } from "../api/formulario";
import { Row, Col, Button, Form } from 'react-bootstrap'

export default function CreateFormulario() {
  const [file, setFile] = useState(null);
  const [id, setId] = useState(1);
  const upload = async () => {
    var formData = new FormData();
    formData.append("file", file);
    try {
      const respuesta = await uploadFile(formData);
      console.log("This is the respuesta", respuesta);
      alert(
        "El archivo xml fue leído correctamente, puedes verificar tu formulario en ver formularios"
      );
    } catch (error) {
      alert(error);
    }
  };

  return (
    <div className="container-login p-3">
      <div>
        <Row>
          <Col column="lg" lg={3}>
            <Form.Label column="lg">
              Sube un archivo
            </Form.Label>
          </Col>
          <Col>
            <Form.Control
              type="file"
              name="archivosubido"
              onChange={(e) => setFile(e.target.files[0])}
            />
          </Col>
        </Row>
      </div>
      <Button type="submit" value="Enviar datos" onClick={() => upload()}>
        Enviar Archivo
      </Button>

    </div>
  );
}
