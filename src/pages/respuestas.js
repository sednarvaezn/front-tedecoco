import React, { useState, useEffect } from "react";
import { Table } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import { getAnswersById } from "../api/formulario";
import "../styles/respuestas.css";
export default function Respuestas() {
  const history = useNavigate();
  const { idFormulario } = useParams();

  const [respuestas, setRespuestas] = useState([]);
  const [headers, setHeaders] = useState([]);
  useEffect(async () => {
    const formulariosd = await getAnswersById(idFormulario);
    setRespuestas(formulariosd);
    let titulos = [];
    console.log(formulariosd);
    Object.keys(formulariosd[0].respuesta).forEach((key) => {
      titulos.push(key);
    });
    setHeaders(titulos);
  }, []);
  return (
    <div style={{ width: "90%", left: "5%", position: "relative" }}>
      <Table responsive striped bordered hover size="sm">
        <tr>
          {headers.map((key) => {
            return <th>{key}</th>;
          })}
        </tr>
        {respuestas.map((respuesta) => {
          return (
            <tr>
              {headers.map((header) => {
                return <td>{respuesta.respuesta[header]}</td>;
              })}
            </tr>
          );
        })}
      </Table>
    </div>
  );
}
