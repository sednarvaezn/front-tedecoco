import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { getAllById } from "../api/formulario";
export default function Select() {
  const history = useNavigate();
  const [user, setUser] = useState(1);
  const [formularios, setFormularios] = useState([]);
  useEffect(async () => {
    const formulariosd = await getAllById(user);
    setFormularios(formulariosd.data);
  }, []);
  return (
    <div>
      {formularios.map((formulario) => {
        return (
          <button
            id={formulario.id}
            onClick={() => history(`/formulario/${formulario.id}`)}
          >
            {formulario.nombre_formulario}
          </button>
        );
      })}
    </div>
  );
}
