import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import "../styles/home.css";
import { Button, Row, Form } from "react-bootstrap";

export default function Home() {
  const [login, setLogin] = useState(false);
  const history = useNavigate();
  return login ? (
    <div className="container-login p-3">
      <Row className="mx-0">
        <Button className="mb-4" onClick={() => history("/select")}>
          Ver Formularios
        </Button>
      </Row>
      <Row className="mx-0">
        <Button className="mb-4" onClick={() => history("/create-formulario")}>
          Crear Nuevo Formulario
        </Button>
      </Row>
      <Row className="mx-0">
        <Button className="m-0" onClick={() => history("/ver-respuestas")}>
          Ver Respuestas
        </Button>
      </Row>
    </div>
  ) : (
    <div className="container-login p-3">
      <div className="pb-3">
        <Row className="mx-0">
          <Form.Control
            className="FormInput"
            type="text"
            placeholder="Usuario"
          />
        </Row>
        <Row className="mx-0">
          <Form.Control
            className="FormInput"
            type="text"
            placeholder="Contraseña"
          />
        </Row>
      </div>
      <Button variant="success" onClick={() => setLogin(true)}>
        Ingresar
      </Button>
    </div>
  );
}
