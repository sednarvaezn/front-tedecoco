import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Button, Row, Form } from 'react-bootstrap'
import { getCampos, create } from "../api/datos";
import "../styles/formulario.css";

export default function Formulario() {
  const [campos, setCampos] = useState([]);
  const { idFormulario } = useParams();
  const [respuestas, setRespuestas] = useState({});

  const submit = async () => {
    const body = {
      form_id: parseInt(idFormulario),
      respuesta: respuestas,
    };
    const respuesta = await create(JSON.stringify(body));
    if (respuesta.status == 201) {
      window.alert("Su respuesta ha sido guardada correctamente");
      window.location.reload();
    }
  };

  const updateRespuesta = (e) => {
    let respuesta = respuestas;
    respuesta[e.target.id] = e.target.value;
    setRespuestas(respuesta);
    console.log(respuestas);
  };
  useEffect(async () => {
    const camposDB = await getCampos(idFormulario);
    console.log(camposDB.user);
    setCampos(camposDB.user);
  }, []);
  return (
    <div className="container-formulario p-3">
      {campos.map((campo) => {
        return (
          <div>
            <Row>{campo.nombre_campo}</Row>
            <Row className="mx-0">
              <Form.Control className="FormInput"
                id={campo.nombre_campo}
                onChange={(e) => updateRespuesta(e)}
                value={respuestas[campo.nombre_campo]}
                type="text"
                placeholder={campo.nombre_campo}
              ></Form.Control>
            </Row>
          </div>
        );
      })}
      <Button type="submit" variant="success" onClick={() => submit()}>
        Registrar
      </Button>
    </div>
  );
}
